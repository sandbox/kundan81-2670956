
*******************************************************************************
*Name: Currency Exchanger                                                     *
*                                                                             *
*Version: 7.x-1.                                                              *
*******************************************************************************

-- SUMMARY --

This project uses Google Finance Currency Converter  to create a 
'Currency Exchanger' page & block that your site visitors can use to find and 
get current currency.
 
-- Requirements --

[Optional]Placeholder module (https://www.drupal.org/project/placeholder).


-- Installing --

Just enable the module.
You should now see currency converter block.
You can also access using this url (/exchange-currency-converter).
